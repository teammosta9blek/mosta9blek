<?php
/**
 * @file
 * feature_roles_droit.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function feature_roles_droit_user_default_roles() {
  $roles = array();

  // Exported role: Business.
  $roles['Business'] = array(
    'name' => 'Business',
    'weight' => 5,
  );

  // Exported role: Student.
  $roles['Student'] = array(
    'name' => 'Student',
    'weight' => 3,
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: university.
  $roles['university'] = array(
    'name' => 'university',
    'weight' => 4,
  );

  return $roles;
}
