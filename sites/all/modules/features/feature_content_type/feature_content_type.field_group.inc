<?php
/**
 * @file
 * feature_content_type.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_content_type_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_g1|node|university|default';
  $field_group->group_name = 'group_g1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'university';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'A vous de noter votre université',
    'weight' => '7',
    'children' => array(
      0 => 'field_question1',
      1 => 'field_star1',
      2 => 'field_question2',
      3 => 'field_star2',
      4 => 'field_question3',
      5 => 'field_star3',
      6 => 'field_question4',
      7 => 'field_star4',
      8 => 'field_question5',
      9 => 'field_star5',
      10 => 'field_question6',
      11 => 'field_star6',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-g1 field-group-fieldset',
        'id' => '',
        'effect' => 'bounceslide',
        'element' => 'div',
        'show_label' => 0,
        'label_element' => 'div',
        'attributes' => '',
        'required_fields' => 1,
        'speed' => 'fast',
      ),
    ),
  );
  $field_groups['group_g1|node|university|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_g4|node|university|default';
  $field_group->group_name = 'group_g4';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'university';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Liste des commentaires',
    'weight' => '8',
    'children' => array(
      0 => 'comments',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Liste des commentaires',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-g4 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_g4|node|university|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_info_university|node|university|form';
  $field_group->group_name = 'group_info_university';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'university';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Information University',
    'weight' => '19',
    'children' => array(
      0 => 'field_raison_sociale',
      1 => 'field_d_nomination_sociale',
      2 => 'field_acronyme',
      3 => 'field_sp_cialit_s',
      4 => 'field_niveau_de_dipl_mes_d_cern_',
      5 => 'field_ann_es_de_cr_ation',
      6 => 'field_nombre_d_tudiants',
      7 => 'field_adresse',
      8 => 'field_code_postale',
      9 => 'field_localit_',
      10 => 'field_gouvernorat',
      11 => 'field_t_l_phone',
      12 => 'field_email',
      13 => 'field_site_web',
      14 => 'field_page_facebook',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Information University',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-info-university field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_info_university|node|university|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_resp_university|node|university|form';
  $field_group->group_name = 'group_resp_university';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'university';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Responsable University',
    'weight' => '20',
    'children' => array(
      0 => 'field_telephone',
      1 => 'field_user',
      2 => 'field_nom',
      3 => 'field_pr_nom',
      4 => 'field_fax',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-resp-university field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_resp_university|node|university|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('A vous de noter votre université');
  t('Information University');
  t('Liste des commentaires');
  t('Responsable University');

  return $field_groups;
}
