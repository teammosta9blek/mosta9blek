<?php
/**
 * @file
 * feature_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function feature_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-facebook.
  $menus['menu-facebook'] = array(
    'menu_name' => 'menu-facebook',
    'title' => 'Facebook',
    'description' => '',
  );
  // Exported menu: menu-liens-utiles.
  $menus['menu-liens-utiles'] = array(
    'menu_name' => 'menu-liens-utiles',
    'title' => 'Liens utiles',
    'description' => '',
  );
  // Exported menu: menu-menu-nouvelles-offres.
  $menus['menu-menu-nouvelles-offres'] = array(
    'menu_name' => 'menu-menu-nouvelles-offres',
    'title' => 'Menu nouvelles offres',
    'description' => '',
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Facebook');
  t('Liens utiles');
  t('Menu nouvelles offres');
  t('The <em>User</em> menu contains links related to the user\'s account, as well as the \'Log out\' link.');
  t('User menu');

  return $menus;
}
