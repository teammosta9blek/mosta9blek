<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any.
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
     <section class="no-padding main-slider height-100 mobile-height wow fadeIn">
            <div class="swiper-full-screen swiper-container height-100 width-100 white-move">
                <div class="swiper-wrapper">
                    <!-- start slider item -->

                     <div  style="background-image: url('/sites/default/files/fond.jpg'); width: 100%;" data-swiper-slide-index="0">
                        
                        <div class="container position-relative full-screen" style="min-height: 800px;">
                            <div class="slider-typography text-center">
                                <div class="slider-text-middle-main">
                                    <div class="slider-text-middle">
                                             <img class="margin-30px-bottom"  src="/sites/default/files/logoMosta9bleck.png" alt=""/>
                    <h6 class="text-very-light-gray padding-ten-lr font-weight-300 margin-two-bottom sm-margin-four-bottom margin-80px-bottom alt-font">Répertoire des établissements privés de l’enseignement supérieurs en Tunisie</h6>
                                        <div class="btn-dual">                      
                                           <div class="container">
                                            <div class="row">
      <?php print $exposed; ?>
        </div></div>    
                                    </div>
                  <div class="down-section text-center"><a href="#work" class="inner-link">
                  <i class="ti-arrow-down icon-extra-small text-black bg-deep-white padding-15px-all xs-padding-10px-all border-radius-100"></i>
                  </a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
            </div>
         </div>  
        </section>



  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
   <section id="work" class="no-padding-bottom wow fadeIn">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- start filter navigation -->
                        <ul class="portfolio-filter nav nav-tabs border-none portfolio-filter-tab-1 alt-font text-uppercase text-center text-small font-weight-600 margin-80px-bottom sm-margin-40px-bottom xs-margin-20px-bottom">
                            <li class="nav active"><a href="javascript:void(0);" data-filter="*" class="xs-display-inline light-gray-text-link text-very-small">All</a></li>
                            <li class="nav"><a href="javascript:void(0);" data-filter=".web" class="xs-display-inline light-gray-text-link text-very-small">Nabeul</a></li>
                            <li class="nav"><a href="javascript:void(0);" data-filter=".advertising" class="xs-display-inline light-gray-text-link text-very-small">Sousse</a></li>
                            <li class="nav"><a href="javascript:void(0);" data-filter=".branding" class="xs-display-inline light-gray-text-link text-very-small">Tunis</a></li>
                            <li class="nav"><a href="javascript:void(0);" data-filter=".design" class="xs-display-inline light-gray-text-link text-very-small">Ariana</a></li>
                            <li class="nav"><a href="javascript:void(0);" data-filter=".photography" class="xs-display-inline light-gray-text-link text-very-small">Manouba</a></li>
                            <li class="nav"><a href="javascript:void(0);" data-filter=".web" class="xs-display-inline light-gray-text-link text-very-small">El Kéf</a></li>
                            <li class="nav"><a href="javascript:void(0);" data-filter=".advertising" class="xs-display-inline light-gray-text-link text-very-small">Béja</a></li>
                            <li class="nav"><a href="javascript:void(0);" data-filter=".branding" class="xs-display-inline light-gray-text-link text-very-small">Mounastir</a></li>
                            <li class="nav"><a href="javascript:void(0);" data-filter=".design" class="xs-display-inline light-gray-text-link text-very-small">Sfax</a></li>
                            <li class="nav"><a href="javascript:void(0);" data-filter=".photography" class="xs-display-inline light-gray-text-link text-very-small">Ben arous</a></li>
                        </ul>                                                                           
                        <!-- end filter navigation -->
                    </div>
                </div>
            </div>
            <div class="container-fluid no-padding">
                <div class="row">
                    <div class="filter-content overflow-hidden padding-15px-lr">
                        <ul class="portfolio-grid work-4col gutter-small hover-option7">
                            <li class="grid-sizer"></li>
                            <?php print $rows; ?>
                            </ul>  
                            </div>
                            </div>
                            </div>                                   
                         </section>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <div class="more">
    <?php print $more; ?>
  </div>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>
