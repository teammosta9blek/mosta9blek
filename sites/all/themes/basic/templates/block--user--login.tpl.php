<?php

/**
 * @file
 */
?>
  <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
                <button class="close-button-menu side-menu-close" id="close-pushmenu"></button>
                <div class="display-table padding-twelve-all height-100 width-100 text-center">
                    <div class="display-table-cell vertical-align-top padding-30px-top position-relative">
                        <div class="row">
            <div class="account-box">
                            <!-- start push menu logo -->
                            <div class="col-lg-12 margin-30px-bottom">
                                <img src="images/logoTunisieEtudeBlue.png" alt="" data-no-retina="">
                            </div>
                            <!-- end push menu logo -->
                            <div class="<?php print $classes; ?>"<?php print $attributes; ?> data-bid="<?php print $block->bid ?>">
                              <?php print render($title_prefix); ?>
                              <?php if ($block->subject): ?>
                                <h3 class="title"<?php print $title_attributes; ?>><?php print $block->subject ?></h3>
                              <?php endif;?>
                              <?php print render($title_suffix); ?>
                              <?php print $content; ?>
                            </div><!-- /block -->
                            <form class="form-signin" action="#">
                <div class="form-group">
                <input type="text" class="form-control" name="user_name" placeholder="Username" required autofocus />
                </div>
                <div class="form-group">
                  <input type="password" class="form-control" placeholder="Password" required />
                </div>
                <div class="form-group">
                <button class="btn btn-lg btn-block" type="submit">Connexion</button>
                </div>
                <div class="form-group">
                <a class="forgotLnk" href="http://www.jquery2dotnet.com">Mot de passe oublié?</a>
                </div>
                </form>                                                          
                    
          <div class="card">
                    <div class="card-content">
                      <div class="card-header-blue">
                           <h6 class="card-heading">Crée votre compte TunisieEtude</h6>
                        </div>
                        <div class="card-body">
                        <p class="card-p">
                                <div class="width-90 margin-lr-auto xs-width-100 xs-no-margin-bottom"><a href="CompteEtudiant.html" style="color:#337ab7">Etudiant : </a> Je crée mon compte</div>
                                <div class="width-90 margin-lr-auto xs-width-100 xs-no-margin-bottom"><a href="CompteEcole.html" style="color:#337ab7">Ecole : </a> Je crée mon compte</div>
                <div class="width-90 margin-lr-auto xs-width-100 xs-no-margin-bottom"><a href="CompteEntreprise.html" style="color:#337ab7">Entreprise : </a> Je crée mon compte</div>
                            </p>
                      </div>                                                                    
                    </div>
                </div>
        <p class="no-margin-bottom font_12">En créant votre compte, vous acceptez l'intégration de nos <span style="color:#337ab7">Conditions générales de vente</span>
        nore Politique de gestion de <span style="color:#337ab7">Vos informations personnelles</span> et notre Politique <span style="color:#337ab7">Cookies et publicité sur Internet.</span>
        </p>
        </div>             
                            <!-- start social links -->
                            <div class="col-md-12 margin-30px-top text-center">
                                <div class="icon-social-medium margin-three-bottom">
                                    <a href="https://www.facebook.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="https://twitter.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                  <a href="https://www.instagram.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        
                                </div>
                <div class="col-md-12 col-xs-12 text-right text-small text-center text-medium-gray">Mentions légales</div>
                            </div>
                            <!-- end social links -->
                        </div>
            
                    </div>
                </div>

            </div>


