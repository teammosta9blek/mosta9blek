<?php

/**
 * @file
 */
?>
<div id="page" class="<?php print $classes; ?>"<?php print $attributes; ?>>

  <!-- ______________________ HEADER _______________________ -->

  <header id="header">
      <?php if ($main_menu || $secondary_menu): ?>
          <nav class="navbar navbar-default bootsnav bg-black header-dark white-link navbar-top">
              <div class="container nav-header-container">
                  <div class="row">
                      <!-- start logo -->
                      <div class="col-md-2 col-xs-5">
                          <?php if ($logo): ?>
                              <a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>" rel="home" id="logo" class="logo">
                                  <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" class="logo-dark"/>
                                  <img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" class="logo-light default"/>
                              </a>
                          <?php endif; ?>
                      </div>
                      <!-- end logo -->
                      <div class="col-md-7 col-xs-2 width-auto pull-right accordion-menu xs-no-padding-right">
                          <button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#navbar-collapse-toggle-1">
                              <span class="sr-only">toggle navigation</span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                              <span class="icon-bar"></span>
                          </button>
                          <div class="navbar-collapse collapse pull-right" id="navbar-collapse-toggle-1">
                              <?php print theme('links', array(
                          'links' => $main_menu,
                          'attributes' => array(
                            'id' => 'main-menu-links',
                            'class' => array('links', 'nav navbar-nav navbar-left no-margin alt-font text-normal'),
                          ),
                          /*'heading' => array(
                            'text' => t('Main menu'),
                            'level' => 'h2',
                            'class' => array('element-invisible'),
                          ),*/
                         ));
                         print theme('links', array(
                                  'links' => $secondary_menu,
                                  'attributes' => array(
                                      'id' => 'secondary',
                                      'class' => array('nav navbar-nav navbar-left panel-group no-margin alt-font font-weight-800'),
                                  ),
                              )); 
                              ?>
                          </div>
                      </div>
                      
                      <div class="col-md-2 col-xs-5 width-auto">
                          <div class="heder-menu-button">
                             <?php if (!user_is_logged_in()) {?>
                          <a href="#" class="navbar-toggle mobile-toggle right-menu-button active alt-font text-normal connexion-button" id="showRightPush">Se connecter</a>
                     
                      <?php  } else {?>
                          <a href="/mos/user/logout" class="navbar-toggle mobile-toggle right-menu-button active alt-font text-normal connexion-button">Se déconnecter</a>
                     <?php }
                      ?>
                             
                          </div>
                      </div>

                      
                  </div>
              </div>
          </nav><!-- /navigation -->

      <?php endif; ?>
  </header><!-- /header -->



  <!-- ______________________ MAIN _______________________ -->

  <div id="main">
    <!--<div class="container">-->
      <section id="content">
        <?php if ($breadcrumb || $title|| $messages || $tabs || $action_links): ?>
          <!-- <div id="content-header"> -->

            <?php print $breadcrumb; ?>

            <?php if ($page['highlighted']): ?>
              <div id="highlighted"><?php print render($page['highlighted']) ?></div>
            <?php endif; ?>

            <?php print render($title_prefix); ?>

            <?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>

            <?php print render($title_suffix); ?>
            <?php print $messages; ?>
            <?php print render($page['help']); ?>

            <?php if (render($tabs)): ?>
              <div class="tabs"><?php print render($tabs); ?></div>
            <?php endif; ?>

            <?php if ($action_links): ?>
              <ul class="action-links"><?php print render($action_links); ?></ul>
            <?php endif; ?>

          <!-- </div> /#content-header -->
        <?php endif; ?>

        <div id="content-area">
          <?php print render($page['content']) ?>
        </div>

        <?php print $feed_icons; ?>

      </section><!-- /content -->

<div id="sidebar_second">   
      <?php if ($page['sidebar_second']): ?>
          <?php print render($page['sidebar_second']); ?>
      <?php endif; ?>
  </div>

      <?php if ($page['sidebar_first']): ?>
        <aside id="sidebar-first">
          <?php print render($page['sidebar_first']); ?>
        </aside>
      <?php endif; ?><!-- /sidebar-first -->

      <!-- /sidebar-second -->
    <!--</div>-->
  </div><!-- /main -->


  <!-- ______________________ FOOTER _______________________ -->

 <footer class="footer-clean  xs-padding-30px-tb"> 
  <hr class="hr-footer">
            <div class="footer-widget-area padding-40px-bottom xs-padding-30px-bottom">
                <div class="container">
                    <div class="row">
    <?php if ($page['footer_firstcolumn'] || $page['footer_secondcolumn'] || $page['footer_thirdcolumn'] || $page['footer_fourthcolumn'] || $page['footer_fivecolumn']): ?>      
        <?php print render($page['footer_fivecolumn']); ?>
        <?php print render($page['footer_firstcolumn']); ?>
        <?php print render($page['footer_secondcolumn']); ?>       
        <?php print render($page['footer_thirdcolumn']); ?>
        <?php print render($page['footer_fourthcolumn']); ?>
    <?php endif; ?>
     </div>
  </div>
</div>
<div class="bg-dark-footer padding-footer-tb text-center xs-padding-30px-tb">
  <div class="container">
<div class="row">
    <?php if ($page['footer']): ?>
    <?php print render($page['footer']); ?>
    <?php endif; ?>
  </div></div>
</div>
</footer>
<div class="right" id="menu-right"> 
<div class="panel-group" id="accordion"> 
                  <div class="panel panel-primary"> 
                    <div class="panel-heading"> 
                      <h4 class="panel-title"> 
                        <div data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> 
                        
                       <span class="glyphicon glyphicon-map-marker"></span>Région<span class="glyphicon glyphicon-menu-down"></div> 
                        
                       </h4>
                     </div>
                    <div id="collapseOne" class="panel-collapse collapse"> 
                       <div class="panel-body"> 
                         <table class="table"> 
                           <tr> 
                             <td> 
                               Nabeul
                            </td> 
                           </tr> 
                           <tr> 
                             <td> 
                               Ariana
                            </td> 
                           </tr> 
                           <tr> 
                            <td> 
                               Tunis
                             </td> 
                           </tr> 
                          <tr> 
                            <td> 
                              Bizert  
                             </td> 
                          </tr> 
                         </table> 
                       </div> 
                     </div> 
                   </div> 
                   <div class="panel panel-primary"> 
                     <div class="panel-heading"> 
                       <h4 class="panel-title"> 
                         <div data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> 
                         <span class="glyphicon glyphicon-align-justify"></span>Domaine<span class="glyphicon glyphicon-menu-down"></div> 
                       </h4> 
                     </div> 
                     <div id="collapseTwo" class="panel-collapse collapse"> 
                       <div class="panel-body"> 
                         <table class="table"> 
                           <tr> 
                             <td> 
                               Informatique
                             </td> 
                           </tr> 
                           <tr> 
                             <td> 
                               Econommie
                             </td> 
                           </tr> 
                           <tr> 
                             <td> 
                              Marketing
                             </td> 
                           </tr> 
                           <tr> 
                             <td> 
                               Gestion
                             </td> 
                           </tr> 
                         </table> 
                       </div> 
                     </div> 
                   </div> 
                   <div class="panel panel-primary"> 
                     <div class="panel-heading">
                       <h4 class="panel-title"> 
                        <div data-toggle="collapse" data-parent="#accordion" href="#collapseThree"> 
                        
                         <span class="glyphicon glyphicon-education"></span>STAGE CV<span class="glyphicon glyphicon-menu-down"></div> 
                       </h4> 
                     </div> 
                     <div id="collapseThree" class="panel-collapse collapse"> 
                       <div class="panel-body"> 
                         <table class="table"> 
                           <tr> 
                             <td> 
                                cv
                             </td> 
                           </tr> 
                           <tr> 
                             <td> 
                               cv 
                             </td> 
                           </tr> 
                           <tr> 
                             <td> 
                               cv
                             </td> 
                           </tr> 
                           <tr> 
                             <td> 
                               cv
                             </td> 
                           </tr> 
                         </table> 
                       </div> 
                     </div> 
                   </div> 
                 </div> 
                </div>
