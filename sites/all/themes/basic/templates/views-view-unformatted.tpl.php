<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
 <li class="col-md-3 grid-item web branding design wow fadeInUp">
    <div class="portfolio-img"><?php print $row; ?></div>
  
  </li>
<?php endforeach; ?>

