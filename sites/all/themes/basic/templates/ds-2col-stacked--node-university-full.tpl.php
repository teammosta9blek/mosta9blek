<?php

/**
 * @file
 * Display Suite 2 column stacked template.
 */
?>
<section class="wow fadeIn">
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds-2col-stacked <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

 <div class="col-md-12">
  <!-- insert picture -->
    <div class="col-md-6 col-sm-12 col-xs-12 margin-30px-bottom wow fadeInLeft" >
           <div class="display-table-cell-vertical-middle md-padding-ten-all sm-padding-six-all xs-padding-50px-tb xs-no-padding-lr">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center">
            <<?php print $left_wrapper ?>>
            <?php print $left; ?>
            </<?php print $left_wrapper ?>>
           </div>
            </div>
           </div>
        </div>
        </div>
  <!-- insert picture -->
    <div class="col-md-6 col-sm-12 col-xs-12 margin-30px-bottom wow fadeInLeft">
                        <div class="display-table-cell-vertical-middle md-padding-ten-all sm-padding-six-all xs-padding-50px-tb xs-no-padding-lr">
                           <div class="container-fluid">
                            <div class="row">
                            <h6 class="alt-font  title-red text-extra-dark-gray sm-text-center margin-eight-bottom sm-margin-40px-bottom xs-margin-30px-bottom font-weight-600">
                            <span class="font-weight-300 display-block xs-margin-15px-bottom"></span>
                               <<?php print $header_wrapper ?> class="group-header<?php print $header_classes; ?>">
                                <?php print $header; ?>
                                </<?php print $header_wrapper ?>>
                          </h6>
                            <div class="panel-group accordion-style2" id="accordion">
                                <!-- start tab content -->
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <div class="panel-title">
                                                
                                                <span class="text-extra-dark-gray xs-width-80 display-inline-block">Proposed Training Programs: </span>
                                                <i class="fa fa-angle-up pull-right text-extra-dark-gray "></i>
                                            </div>
                                        </a>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in">
                                        <div class="panel-body">                
                                        <<?php print $right_wrapper ?> class="group-right<?php print $right_classes; ?>">
                                        <?php print $right; ?>
                                        </<?php print $right_wrapper ?>>
                                        </div>
                                    </div>
                                    </div>
                            </div>
                            </div>
          </div>
          </div>
          </div> 
      <div class="col-md-12 col-sm-12 col-xs-12  margin-eight-bottom margin-30px-bottom wow fadeIn Left">
          <div class="display-table-cell-vertical-middle md-padding-ten-all sm-padding-six-all xs-padding-50px-tb xs-no-padding-lr">
            <<?php print $footer_wrapper ?> class="group-footer<?php print $footer_classes; ?>">
            <?php print $footer; ?>
            </<?php print $footer_wrapper ?>>
           </div>
           </div>
</div>
</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
</section>




