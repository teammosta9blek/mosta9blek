(function ($) {
    Drupal.behaviors.all = {
      attach: function (context, settings) {
        // Code to be run on page load, and  on ajax load added here

        /* Code for the home page Filter*/
        $("#edit-field-r-gion-tid option[value='All']").remove();
        $("#edit-field-tag-tid option[value='All']").remove();

        /* Code for the filter of the home_page*/
        $('#views-exposed-form-frontpage-page', context).once('all', function () {
            // Code here will only be applied to $('#views-exposed-form-frontpage-page') 
            // a single time.
            $( "#views-exposed-form-frontpage-page #edit-field-tag-tid-wrapper" ).after(function() {
                return '<div id="stagecv" class="views-exposed-widget">'+
                '<div class="views-widget">'+
                '<div class="form-item form-item-field-r-gion-tid form-type-select form-group">'+
                '<select class="form-control form-select" name="stage-cv">'+
                '<option value="none" selected="selected">STAGE CV</option>'+
                '<option value="etudiant">Demande de stage (étudiant)</option>'+
                '<option value="company">Offre de stage (entreprise)</option>'+
                '<option value="cv">Déposez CV (étudiant)</option>'+
                '</select></div></div></div>';
              });
        });

        /* Code for the filter of the right_menu*/
        $('#views-exposed-form-frontpage-menu-right-filter', context).once('all', function () {
            // Code here will only be applied to $('#views-exposed-form-frontpage-menu-right-filter') 
            // a single time.
              $( "#views-exposed-form-frontpage-menu-right-filter #edit-field-tag-tid-wrapper" ).after(function() {
                return '<div id="stagecv" class="views-exposed-widget">'+
                '<div class="views-widget">'+
                '<div class="form-item form-item-field-r-gion-tid form-type-select form-group">'+
                '<select class="form-control form-select" name="stage-cv">'+
                '<option value="none" selected="selected">STAGE CV</option>'+
                '<option value="etudiant">Demande de stage (étudiant)</option>'+
                '<option value="company">Offre de stage (entreprise)</option>'+
                '<option value="cv">Déposez CV (étudiant)</option>'+
                '</select></div></div></div>';
              });

            // listen for scroll
            var positionElementInPage = $('#menu_right').offset().top;
            $(window).scroll(
                function() {
                    if ($(window).scrollTop() == positionElementInPage) {
                        // fixed
                        $('#menu_right').addClass("floatable");
                    } else {
                        // relative
                        $('#menu_right').removeClass("floatable");
                    }
                }
            );
        });

        /* Code for the filter of List_University*/
        $("#views-exposed-form-frontpage-list-university .form-item-field-r-gion-tid .form-item-edit-field-r-gion-tid-43").remove();
      }
    };
  }(jQuery));
