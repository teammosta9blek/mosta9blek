<?php
/*Template Name: FlexSlider Slideshow*/
// Add js and css
//dpm($data['field_image_slide']);
 print render($content['field_image_slide']); 
//kint($node->get('field_image_slide')->entity);
/*drupal_add_js("sites/all/libraries/flexslider/js/jquery.js");
drupal_add_js("sites/all/libraries/flexslider/js/modernizr.js");
drupal_add_js("sites/all/libraries/flexslider/js/bootstrap.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/jquery.easing.1.3.js");
drupal_add_js("sites/all/libraries/flexslider/js/smooth-scroll.js");
drupal_add_js("sites/all/libraries/flexslider/js/jquery.appear.js");
drupal_add_js("sites/all/libraries/flexslider/js/bootsnav.js");
drupal_add_js("sites/all/libraries/flexslider/js/jquery.nav.js");
drupal_add_js("sites/all/libraries/flexslider/js/wow.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/page-scroll.js");
drupal_add_js("sites/all/libraries/flexslider/js/swiper.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/jquery.count-to.js");
drupal_add_js("sites/all/libraries/flexslider/js/jquery.stellar.js");
drupal_add_js("sites/all/libraries/flexslider/js/jquery.magnific-popup.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/isotope.pkgd.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/imagesloaded.pkgd.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/classie.js");
drupal_add_js("sites/all/libraries/flexslider/js/hamburger-menu.js");
drupal_add_js("sites/all/libraries/flexslider/js/counter.js");
drupal_add_js("sites/all/libraries/flexslider/js/jquery.fitvids.js");
drupal_add_js("sites/all/libraries/flexslider/js/equalize.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/skill.bars.jquery.js");
drupal_add_js("sites/all/libraries/flexslider/js/justified-gallery.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/jquery.easypiechart.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/instafeed.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/retina.min.js");
drupal_add_js("sites/all/libraries/flexslider/js/main.js");


drupal_add_js("sites/all/libraries/revolution/js/jquery.themepunch.tools.min.js");
drupal_add_js("sites/all/libraries/revolution/js/jquery.themepunch.revolution.min.js");

*/
drupal_add_js("//code.jquery.com/jquery-1.11.1.min.js");
drupal_add_js("//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js");
drupal_add_css("//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css");

?>

<!-- Place somewhere in the <body> of your page -->



<div id="mycarousel" class="no-padding main-slider height-100 mobile-height wow fadeIn carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <!--<ol class="carousel-indicators">
    <li data-target="#mycarousel" data-slide-to="0" class="active"></li>
    <li data-target="#mycarousel" data-slide-to="1"></li>
    <li data-target="#mycarousel" data-slide-to="2"></li>
    <li data-target="#mycarousel" data-slide-to="3"></li>
    <li data-target="#mycarousel" data-slide-to="4"></li>
  </ol>-->

  <!-- Wrapper for slides -->

  <div class="carousel-inner" role="listbox">
  	<?php foreach ($data as $key => $item) { 
    print '<div class="item">';
   	print '<img src="'.$item['path'].'" />';  ?>
   	   <div class="container position-relative full-screen">						
        <div class="slider-typography text-center">
			<div class="slider-text-middle-main">
                <div class="slider-text-middle">  
					<img class="margin-30px-bottom"  src="images/logoTunisieEtude.png" alt=""/>
					<h6 class="text-very-light-gray padding-ten-lr font-weight-300 margin-two-bottom sm-margin-four-bottom margin-80px-bottom alt-font">Répertoire des établissements privés de l’enseignement supérieurs en Tunisie</h6>
                        <div class="btn-dual">											
                        <div class="container">
						<div class="row">
						<div class="col-lg-4">
						<!-- Split button -->
						<div class="select_slider">
						<select name="ANNEE_ETUDE" id="ANNEE_ETUDE" class="bg-transparent-white no-margin-bottom">
							<option label="Etablissments triés par région">Etablissments triés par région</option>								
							<option value="$500 - $1000">$500 - $1000</option>
							<option value="$1000 - $2000">$1000 - $2000</option>
							<option value="$2000 - $5000">$2000 - $5000</option>											
							</select>
							</div>
							</div><!-- /.col-lg-6 -->	
  
							<div class="col-lg-4">
							<!-- Split button -->
							<div class="select_slider">
							<select name="ANNEE_ETUDE" id="ANNEE_ETUDE" class="bg-transparent-white no-margin-bottom">
							<option label="Domaine de formation">Domaine de formation</option>												
							<option value="$500 - $1000">$500 - $1000</option>
							<option value="$1000 - $2000">$1000 - $2000</option>
							<option value="$2000 - $5000">$2000 - $5000</option>
							</select>
							</div>
							</div><!-- /.col-lg-6 -->	
							<div class="col-lg-4">
							<!-- Split button -->
							<div class="select_slider">
							<select name="ANNEE_ETUDE" id="ANNEE_ETUDE" class="bg-transparent-white no-margin-bottom">
							<option label="STAGE CV">STAGE CV</option>												
							<option value="$500 - $1000">$500 - $1000</option>
							<option value="$1000 - $2000">$1000 - $2000</option>
							<option value="$2000 - $5000">$2000 - $5000</option>
							</select>
							</div>
							</div><!-- /.col-lg-6 -->				
							</div>	
                            </div>
							<div class="down-section text-center"><a href="#about" class="inner-link">
							<i class="ti-arrow-down icon-extra-small text-black bg-deep-white padding-15px-all xs-padding-10px-all border-radius-100"></i>
							</a>
							</div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- end slider item -->
                    
                </div>
    </div>
    <?php } ?>
  </div>
  <!-- Controls -->
  <a class="left carousel-control" href="#mycarousel" role="button" data-slide="prev">
    <!--<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>-->
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#mycarousel" role="button" data-slide="next">
      <!--<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>-->
    <span class="sr-only">Next</span>
  </a>
</div>
<script type="text/javascript">
    var $item = $('.carousel .item'); 
var $wHeight = $(window).height();
$item.eq(0).addClass('active');
$item.height($wHeight); 
$item.addClass('full-screen');

$('.carousel img').each(function() {
  var $src = $(this).attr('src');
  var $color = $(this).attr('data-color');
  $(this).parent().css({
    'background-image' : 'url(' + $src + ')',
    'background-color' : $color
  });
  $(this).remove();
});

$(window).on('resize', function (){
  $wHeight = $(window).height();
  $item.height($wHeight);
});

$('.carousel').carousel({
  interval: 6000,
  pause: "false"
});
</script>

<style type="text/css">
.full-screen {
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
}
</style>







